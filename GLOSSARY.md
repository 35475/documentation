## Virtual Machine Instance
Server-like resource in OpenStack

## Security Group
Firewall-like resource in OpenStack

## Project
Resource ownership unit in OpenStack

## Floating IP Address
Public/external IP resource in OpenStack

## Image
Operating system resource in OpenStack

## Volume
Storage allocation resource in OpenStack

## SSH
Secure Shell

## SSH Key Pair
Set of keys for asymmetric cryptography, used for secure remote access

## User Support
[cloud@metacentrum.cz](mailto:cloud@metacentrum.cz)

## MUNI Identity Management
[idm@ics.muni.cz](mailto:idm@ics.muni.cz)

## VO
Virtual Organization, unit of organizational hierarchy
