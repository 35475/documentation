# How To Migrate

Migration from legacy platforms `CESNET-MetaCloud` (OpenNebula) and
`OStack ICS MUNI` will be handled internally on a case-by-case basis.

Users who do not require assistance should terminate their virtual machines
in legacy platforms and start new virtual machines in MetaCentrum Cloud
at their own convenience.

Remaining users and larger communities will be contacted in the near
future for assisted migration.

Migration will follow these general rules
* running virtual machines cannot be transferred,
* public IP addresses cannot be transferred,
* images with operating systems cannot be transferred.

If you have any questions, please contact user support.
