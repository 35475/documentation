# Accessing Windows instance
Windows host system for has RDP allowed for `Administrators` group. By default there are two users in this group: 
- Admin - the password for this account is filled with `admin_pass` metadata in OpenStack, if no value is entered for this key, random password is generated. (could be used for orchestartion).
- Administrator - the password must be filled after instantiation of the system.

The next step is to create a security group, that will allow access to a port `3389` ([RDP protocol](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol)) for the instance. 

We recommend disabling those accounts, creating new ones in order to administer Windows instance in any production environment.


# Licensing 
*TBD*

# Advanced users
- You may use all features of [cloudbase-init](https://cloudbase.it/cloudbase-init/) for Windows.
- Windows Server [hardening guidelines](https://security.uconn.edu/server-hardening-standard-windows/).