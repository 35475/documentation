# IMPORTANT news
This page deals with the common rules for usage of the Cloud2 metacentrum - OpenStack.

1. Data stored in the cloud are not being backed up, therefore any user has to deals with backups outside of the cloud.
2. User projects (generated for every user) are not meant to contain production machines. Any machine contained in the personal project may be terminated at any time without previous warnings. If you use your personal project for long-term services, you has to ask for a GROUP project (even if you do not work in a group or you do not need any extra quotas).
3. Networks:
  1. If you are using a PERSONAL project you has to use the `78-128-250-pers-proj-net` network. Use `public-cesnet-78-128-250-PERSONAL` for FIP allocation, FIPs from this pool will be periodically released.
  2. If you are using a GROUP project you may choose from the `public-cesnet-78-128-251-GROUP`  and `public-muni-147-251-124-GROUP`.
  3. Violation of the network usage may lead into resource removal and reducing of the quotas assigned.
